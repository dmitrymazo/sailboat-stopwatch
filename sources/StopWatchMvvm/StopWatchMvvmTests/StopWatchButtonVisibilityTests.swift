//
//  StopWatchButtonVisibilityTests.swift
//  MvvmArchitectureTests
//
//  Created by Dmitry Mazo on 5/8/21.
//

import XCTest
@testable
import StopWatchMvvm

final class StopWatchButtonVisibilityTests: XCTestCase {
    
    private var scheduler = MockScheduler(step: TestParameters.step)
    private var stopWatch: StopWatchViewModel!
    
    override func setUp() {
        stopWatch = StopWatchViewModel(step: TestParameters.step, scheduler: scheduler)
    }
    
    func testUninitializedState() {
        XCTAssertEqual(stopWatch.isStartButtonHidden.value, false)
        XCTAssertEqual(stopWatch.isPauseButtonHidden.value, true)
        XCTAssertEqual(stopWatch.isResumeButtonHidden.value, true)
        XCTAssertEqual(stopWatch.isResetButtonHidden.value, true)
    }
    
    func testStartedState() {
        stopWatch.start()
        
        XCTAssertEqual(stopWatch.isStartButtonHidden.value, true)
        XCTAssertEqual(stopWatch.isPauseButtonHidden.value, false)
        XCTAssertEqual(stopWatch.isResumeButtonHidden.value, true)
        XCTAssertEqual(stopWatch.isResetButtonHidden.value, false)
    }
    
    func testPausedState() {
        stopWatch.start()
        stopWatch.pause()
        
        XCTAssertEqual(stopWatch.isStartButtonHidden.value, true)
        XCTAssertEqual(stopWatch.isPauseButtonHidden.value, true)
        XCTAssertEqual(stopWatch.isResumeButtonHidden.value, false)
        XCTAssertEqual(stopWatch.isResetButtonHidden.value, false)
    }
    
    func testResumedState() {
        stopWatch.start()
        stopWatch.pause()
        stopWatch.resume()
        
        XCTAssertEqual(stopWatch.isStartButtonHidden.value, true)
        XCTAssertEqual(stopWatch.isPauseButtonHidden.value, false)
        XCTAssertEqual(stopWatch.isResumeButtonHidden.value, true)
        XCTAssertEqual(stopWatch.isResetButtonHidden.value, false)
    }
    
}
