//
//  MockTimeMeasurer.swift
//  MvvmArchitectureTests
//
//  Created by Dmitry Mazo on 5/8/21.
//

import Foundation
@testable
import StopWatchMvvm

final class MockScheduler: SchedulerProtocol {
    weak var delegate: SchedulerDelegate?
    
    private(set) var isRunning = false
    
    private var step: TimeInterval
    
    func start() {
        isRunning = true
    }
    
    func reset() {
        isRunning = false
    }
    
    // MARK: - Mock
    
    func setElapsed(_ time: TimeInterval) {
        let iterations = Int(time / step)
        for _ in 0..<iterations {
            delegate?.schedulerFired()
        }
    }
    
    init(step: TimeInterval) {
        self.step = step
    }
}
