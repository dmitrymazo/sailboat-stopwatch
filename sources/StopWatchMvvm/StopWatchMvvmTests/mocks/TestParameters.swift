//
//  TestParameters.swift
//  StopWatchMvvmTests
//
//  Created by Dmitry Mazo on 5/9/21.
//  Copyright © 2021 Sailboat Inc. All rights reserved.
//

import Foundation

struct TestParameters {
    static let step = 1.0
}
