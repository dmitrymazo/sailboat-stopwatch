//
//  StopWatchTimeTests.swift
//  MvvmArchitectureTests
//
//  Created by Dmitry Mazo on 5/8/21.
//

import XCTest
@testable
import StopWatchMvvm

final class StopWatchTimeTests: XCTestCase {
    
    private var scheduler = MockScheduler(step: TestParameters.step)
    private var stopWatch: StopWatchViewModel!
    
    override func setUp() {
        stopWatch = StopWatchViewModel(step: TestParameters.step, scheduler: scheduler)
    }
    
    func testInitialTimeIsZero() {
        XCTAssertEqual(stopWatch.time.value, 0)
    }
    
    func testStartComponent() {
        stopWatch.start()
        XCTAssertEqual(scheduler.isRunning, true)
    }
    
    func testPauseComponent() {
        stopWatch.start()
        scheduler.setElapsed(10)
        stopWatch.pause()
        
        XCTAssertEqual(scheduler.isRunning, false)
        XCTAssertEqual(stopWatch.time.value, 10)
    }
    
    func testResumeComponent() {
        stopWatch.start()
        scheduler.setElapsed(30)
        stopWatch.pause()
        stopWatch.resume()
        scheduler.setElapsed(10)
        stopWatch.pause()
        stopWatch.resume()
        scheduler.setElapsed(10)
        
        XCTAssertEqual(scheduler.isRunning, true)
        XCTAssertEqual(stopWatch.time.value, 50)
    }
    
    func testResetComponent() {
        stopWatch.start()
        scheduler.setElapsed(20)
        
        stopWatch.reset()
        XCTAssertEqual(scheduler.isRunning, false)
        XCTAssertEqual(stopWatch.time.value, 0)
    }
    
}
