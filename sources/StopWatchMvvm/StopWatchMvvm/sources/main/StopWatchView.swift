//
//  StopWatchView.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//

import UIKit
import Common

final class StopWatchView: UIView {
    let timeLabel = TimeLabel()
    let controlPanelView = ControlPanelView()
    
    init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = .darkGray
        self.addSubview(timeLabel)
        self.addSubview(controlPanelView)
        
        NSLayoutConstraint.activate([
            timeLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 80),
            timeLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            //
            controlPanelView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            controlPanelView.topAnchor.constraint(equalTo: timeLabel.bottomAnchor, constant: 80)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
