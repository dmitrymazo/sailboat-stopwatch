//
//  StopWatchViewModel.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//

import Foundation
import Common

protocol StopWatchViewModelProtocol {
    // Output
    var time: Observable<TimeInterval> { get }
    var isStartButtonHidden: Observable<Bool> { get }
    var isPauseButtonHidden: Observable<Bool> { get }
    var isResumeButtonHidden: Observable<Bool> { get }
    var isResetButtonHidden: Observable<Bool> { get }
    
    // Input
    func start()
    func pause()
    func resume()
    func reset()
}

final class StopWatchViewModel: StopWatchViewModelProtocol {
    
    private(set) var time = Observable(0.0)
    
    private(set) var isStartButtonHidden = Observable(false)
    private(set) var isPauseButtonHidden = Observable(true)
    private(set) var isResumeButtonHidden = Observable(true)
    private(set) var isResetButtonHidden = Observable(true)
    
    private var state = StopWatchState.uninitialized
    private var scheduler: StopWatchScheduler?
    
    private func updateButtonVisibility() {
        switch state {
        case .uninitialized:
            isStartButtonHidden.value = false
            isPauseButtonHidden.value = true
            isResumeButtonHidden.value = true
            isResetButtonHidden.value = true
        case .started,
             .resumed:
            isStartButtonHidden.value = true
            isPauseButtonHidden.value = false
            isResumeButtonHidden.value = true
            isResetButtonHidden.value = false
        case .paused:
            isStartButtonHidden.value = true
            isPauseButtonHidden.value = true
            isResumeButtonHidden.value = false
            isResetButtonHidden.value = false
        }
    }
    
    // MARK: - Internal
    
    func start() {
        guard state == .uninitialized else { return }
        state = .started
        scheduler?.start()
        updateButtonVisibility()
    }
    
    func pause() {
        guard [.started, .resumed].contains(state) else { return }
        state = .paused
        scheduler?.pause()
        updateButtonVisibility()
    }
    
    func resume() {
        guard state == .paused else { return }
        state = .resumed
        scheduler?.start()
        updateButtonVisibility()
    }
    
    func reset() {
        state = .uninitialized
        scheduler?.reset()
        updateButtonVisibility()
    }
    
    // MARK: - Init
    
    init(step: TimeInterval, scheduler: SchedulerProtocol) {
        self.scheduler = StopWatchScheduler(step: step, scheduler: scheduler)
        
        self.scheduler?.time.observe { [weak self] time in
            self?.time.value = time
        }
    }
    
    deinit {
        scheduler?.reset()
    }
    
}

private enum StopWatchState {
    case uninitialized
    case started
    case paused
    case resumed
}
