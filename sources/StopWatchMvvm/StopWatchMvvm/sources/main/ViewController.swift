//
//  ViewController.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/5/21.
//

import UIKit
import Common

final class ViewController: UIViewController {
    
    private static let timeInterval = 0.01
    
    private let stopWatch: StopWatchController = {
        let scheduler = Scheduler(timeInterval: ViewController.timeInterval)
        let viewModel = StopWatchViewModel(step: ViewController.timeInterval, scheduler: scheduler)
        let controller = StopWatchController(viewModel: viewModel)
        
        return controller
    }()
    
    // MARK: - Internal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let stopWatchView = stopWatch.contentView
        
        self.view.addSubview(stopWatchView)
        
        NSLayoutConstraint.activate([
            stopWatchView.topAnchor.constraint(equalTo: self.view.topAnchor),
            stopWatchView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            stopWatchView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            stopWatchView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        ])
    }
    
}



