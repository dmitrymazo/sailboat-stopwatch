//
//  StopWatchController.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//

import Foundation
import Common

final class StopWatchController {
    let contentView = StopWatchView()
    
    private let viewModel: StopWatchViewModelProtocol
    
    private func bind() {
        self.viewModel.time.observe { [weak self] time in
            DispatchQueue.main.async {
                self?.contentView.timeLabel.text = StopWatchTextGenerator.text(from: time)
            }
        }
        
        self.viewModel.isStartButtonHidden.observe { [weak self] isHidden in
            DispatchQueue.main.async {
                self?.contentView.controlPanelView.startButton.isHidden = isHidden
            }
        }
        
        self.viewModel.isPauseButtonHidden.observe { [weak self] isHidden in
            DispatchQueue.main.async {
                self?.contentView.controlPanelView.pauseButton.isHidden = isHidden
            }
        }
        
        self.viewModel.isResumeButtonHidden.observe { [weak self] isHidden in
            DispatchQueue.main.async {
                self?.contentView.controlPanelView.resumeButton.isHidden = isHidden
            }
        }
        
        self.viewModel.isResetButtonHidden.observe { [weak self] isHidden in
            DispatchQueue.main.async {
                self?.contentView.controlPanelView.resetButton.isHidden = isHidden
            }
        }
    }
    
    private func setupEvents() {
        contentView.controlPanelView.startButton.addTarget(self, action: #selector(startTapped), for: .touchUpInside)
        contentView.controlPanelView.pauseButton.addTarget(self, action: #selector(pauseTapped), for: .touchUpInside)
        contentView.controlPanelView.resumeButton.addTarget(self, action: #selector(resumeTapped), for: .touchUpInside)
        contentView.controlPanelView.resetButton.addTarget(self, action: #selector(resetTapped), for: .touchUpInside)
    }
    
    @objc
    private func startTapped() {
        viewModel.start()
    }
    
    @objc
    private func pauseTapped() {
        viewModel.pause()
    }
    
    @objc
    private func resumeTapped() {
        viewModel.resume()
    }
    
    @objc
    private func resetTapped() {
        viewModel.reset()
    }
    
    init(viewModel: StopWatchViewModelProtocol) {
        self.viewModel = viewModel
        self.bind()
        self.setupEvents()
    }
}
