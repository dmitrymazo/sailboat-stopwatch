//
//  Time.swift
//  Common
//
//  Created by Dmitry Mazo on 5/10/21.
//  Copyright © 2021 Sailboat Inc. All rights reserved.
//

import Foundation

public struct Time {
    let hours: Int
    let minutes: Int
    let seconds: Int
    let milliSeconds: Int
    
    init(hours: Int, minutes: Int, seconds: Int, milliSeconds: Int = 0) {
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds
        self.milliSeconds = milliSeconds
    }
    
    init(seconds: TimeInterval) {
        let decimalSeconds = Int(seconds)
        
        let milliSecondsInSecond = 1000.0
        let milliSeconds = seconds.truncatingRemainder(dividingBy: 1) * milliSecondsInSecond
        
        self.hours = decimalSeconds / 3600
        self.minutes = (decimalSeconds % 3600) / 60
        self.seconds = (decimalSeconds % 3600) % 60
        self.milliSeconds = Int(milliSeconds)
    }
}
