//
//  StopWatchTextGenerator.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//  Copyright © 2021 Sailboat Inc. All rights reserved.
//

import Foundation

public final class StopWatchTextGenerator {
    public static func text(from time: TimeInterval) -> String {
        let timeObj = Time(seconds: time)
        
        let minutes = String(format: "%02d", timeObj.minutes)
        let seconds = String(format: "%02d", timeObj.seconds)
        let milliSeconds = String(format: "%02d", timeObj.milliSeconds / 10)
        
        return "\(minutes):\(seconds),\(milliSeconds)"
    }
}
