//
//  Scheduler.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//  Copyright © 2021 Sailboat Inc. All rights reserved.
//

import Foundation

public protocol SchedulerProtocol {
    var delegate: SchedulerDelegate? { get set }
    func start()
    func reset()
}

public final class Scheduler: SchedulerProtocol {
    
    private var timer: Timer?
    private let timeInterval: TimeInterval
    
    public weak var delegate: SchedulerDelegate?
    
    @objc
    private func timerFired() {
        delegate?.schedulerFired()
    }
    
    public func start() {
        timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(timerFired), userInfo: nil, repeats: true)
    }
    
    public func reset() {
        timer?.invalidate()
    }
    
    public init(timeInterval: TimeInterval) {
        self.timeInterval = timeInterval
    }
    
    deinit {
        timer?.invalidate()
    }
    
}

public protocol SchedulerDelegate: AnyObject {
    func schedulerFired()
}
