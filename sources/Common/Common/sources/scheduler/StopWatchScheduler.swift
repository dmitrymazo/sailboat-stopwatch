//
//  StopWatchScheduler.swift
//  StopWatchMvvm
//
//  Created by Dmitry Mazo on 5/9/21.
//  Copyright © 2021 Sailboat Inc. All rights reserved.
//

import Foundation

public final class StopWatchScheduler: SchedulerDelegate {
    public private(set) var time = Observable(0.0)
    
    private var scheduler: SchedulerProtocol?
    private let step: TimeInterval
    private var previousTime = 0.0
    private var newIterationTime = 0.0
    
    private func updateTime() {
        time.value = previousTime + newIterationTime
    }
    
    public func start() {
        scheduler?.start()
    }
    
    public func pause() {
        previousTime += newIterationTime
        newIterationTime = 0
        scheduler?.reset()
    }
    
    public func resume() {
        scheduler?.start()
    }
    
    public func reset() {
        previousTime = 0
        newIterationTime = 0
        updateTime()
        scheduler?.reset()
    }
    
    public func schedulerFired() {
        self.newIterationTime += step
        updateTime()
    }
    
    public init(step: TimeInterval, scheduler: SchedulerProtocol) {
        self.step = step
        self.scheduler = scheduler
        self.scheduler?.delegate = self
    }
}
