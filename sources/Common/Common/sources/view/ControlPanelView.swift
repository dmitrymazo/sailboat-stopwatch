//
//  ControlPanelView.swift
//  Common
//
//  Created by Dmitry Mazo on 5/10/21.
//  Copyright © 2021 Sailboat Inc. All rights reserved.
//

import UIKit

public final class ControlPanelView: UIView {
    public let startButton: UIButton = {
        let button = ControlPanelButton()
        button.setTitle("Start", for: .normal)
        
        return button
    }()
    
    public let pauseButton: UIButton = {
        let button = ControlPanelButton()
        button.setTitle("Pause", for: .normal)
        
        return button
    }()
    
    public let resumeButton: UIButton = {
        let button = ControlPanelButton()
        button.setTitle("Resume", for: .normal)
        
        return button
    }()
    
    public let resetButton: UIButton = {
        let button = ControlPanelButton()
        button.setTitle("Reset", for: .normal)
        
        return button
    }()
    
    private let stackview = UIStackView()
    
    public init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let buttons = [
            startButton,
            pauseButton,
            resumeButton,
            resetButton
        ]
        
        var const = [NSLayoutConstraint]()
        buttons.forEach { button in
            const += [
                button.widthAnchor.constraint(equalToConstant: 100),
                button.heightAnchor.constraint(equalToConstant: 60)
            ]
            stackview.addArrangedSubview(button)
        }
        
        NSLayoutConstraint.activate(const)
        
        stackview.axis = .horizontal
        stackview.distribution = .equalSpacing
        stackview.alignment = .center
        stackview.spacing = 16.0
        stackview.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(stackview)
        
        NSLayoutConstraint.activate([
            stackview.topAnchor.constraint(equalTo: self.topAnchor),
            stackview.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            stackview.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackview.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private final class ControlPanelButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setTitleColor(.white, for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        self.backgroundColor = .gray
        self.layer.cornerRadius = 12
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
