//
//  TimeLabel.swift
//  Common
//
//  Created by Dmitry Mazo on 5/10/21.
//  Copyright © 2021 Sailboat Inc. All rights reserved.
//

import UIKit

public final class TimeLabel: UILabel {
    public init() {
        super.init(frame: .zero)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.textColor = .white
        self.font = UIFont.monospacedSystemFont(ofSize: 40, weight: .medium)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
